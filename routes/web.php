<?php

use Illuminate\Support\Facades\Route;

use app\Http\Controllers\PagesController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get("/blog", function(){
    return view('/layouts/blog');
})->name('blog');

Route::get('/single', function () {
    return view('/layouts/single');
})->name('single');

Route::get("/contact", function(){
    return view('/layouts/Contact_us');
})->name('contact');

Route::get("/admin", function(){
    return view('/admin/welcome');
})->name('article');

Route::get("/admin/dashboard", function(){
    return view('/admin/dashboard');
})->name('dashboard');
